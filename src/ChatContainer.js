import React, { Component } from "react";
import "./App.css";
import InputMessage from "./InputMessage";
import Messages from "./Messages";

export default class ChatContainer extends Component {
  sendMessage = text => {
    this.props.onsend(text);
  };

  render() {
    return !this.props.room ? (
      <div className="chatContainer">
        <h1>{`Welcome ${getname(this.props.loggedInUser)}`}</h1>
        <p>
          Click on any friend from the friendlist to start chatting with him
        </p>
      </div>
    ) : (
      <div className="chatContainer">
        <div className="currentChat">
          <div className="chatHeader">
            {`${this.props.loggedInUser.name} Talking to ${
              this.props.chatActiveUser.name
            }`}
          </div>
        </div>
        <Messages
          curerentUserId={this.props.loggedInUser.id}
          currentRoom={this.props.room}
          messages={this.props.messages}
        />
        <InputMessage sendMessage={this.sendMessage} />
      </div>
    );
  }
}

function getname(user) {
  if (user) {
    return user.name;
  }
  return "";
}
