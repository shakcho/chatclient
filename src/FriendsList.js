import React, { Component } from "react";
import Friend from "./Friend";
import "./App.css";

export default class FriendsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
      chatActiveFriendId: ""
    };
    this.handleSearch = this.handleSearch.bind(this);
  }
  handleSearch = event => {
    this.setState({
      searchTerm: event.target.value
    });
  };

  handelClick = id => {
    this.setState({
      chatActiveFriendId: id
    });
    this.props.chatRoom(id);
  };

  render() {
    return (
      <div className="friendList">
        <input
          className="searchBox"
          name="searchFriend"
          onChange={this.handleSearch}
          placeholder="Search Friend"
        />
        {this.props.friendsList
          .filter(
            item =>
              item.name
                .toUpperCase()
                .indexOf(this.state.searchTerm.toUpperCase()) >= 0 &&
              item.id !== this.props.currentUser.toString()
          )
          .map(item => {
            const activeClass = this.state.chatActiveFriendId === item.id;
            return (
              <Friend
                key={item.id.toString()}
                name={item.name}
                avatar={item.avatar}
                chatActive={activeClass}
                handelClick={() => this.handelClick(item.id.toString())}
              />
            );
          })}
      </div>
    );
  }
}
