import React, { Component } from "react";
import "./App.css";

export default class Friend extends Component {
  render() {
    const activeChatCss = this.props.chatActive ? "active" : "";
    return (
      <div
        className={`friend ${activeChatCss}`}
        onClick={this.props.handelClick}
      >
        <img src={this.props.avatar} alt={this.props.name} />
        <p className="friend-data">{this.props.name}</p>
      </div>
    );
  }
}
