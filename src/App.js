import React, { Component } from "react";
import "./App.css";
import InitPage from "./InitPage";
import FriendList from "./FriendsList";
import ChatContainer from "./ChatContainer";
import axios from "axios";
const wsUri = "ws://localhost:8999";
const websocket = new WebSocket(wsUri);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      currentUser: "",
      friendsList: [],
      currentRoom: "",
      chatActiveUser: ""
    };
  }

  componentDidMount() {
    axios.get("./data.json").then(res => {
      const friendsList = res.data.friendsList;
      this.setState({ friendsList });
    });
    this.testWebSocket();
  }

  handleUserChange = currentUserId => {
    this.setState({ currentUser: currentUserId });
  };

  testWebSocket = () => {
    websocket.onopen = evt => {
      this.onOpen(evt);
    };
    websocket.onclose = evt => {
      this.onClose(evt);
    };
    websocket.onmessage = evt => {
      this.onMessage(evt);
    };
    websocket.onerror = evt => {
      this.onError(evt);
    };
  };

  onOpen = event => {
    console.log("CONNECTED");
  };
  onClose = event => {
    console.log("DISCONNECTED");
  };

  onMessage = event => {
    let mArray = this.state.messages.concat([JSON.parse(event.data)]);
    this.setState({
      messages: mArray
    });
  };

  onError = event => {
    console.log(event.data);
  };

  doSend = message => {
    if (message.length > 0) {
      let newMessage = {
        id: makeid(),
        message: message,
        sender: this.state.currentUser,
        roomId: this.state.currentRoom
      };
      websocket.send(JSON.stringify(newMessage));
      let mArray = this.state.messages.concat([newMessage]);
      this.setState({
        messages: mArray
      });
    }
  };

  initChatRoom = friendId => {
    let roomId = generateRoomId(friendId, this.state.currentUser);
    this.setState({ currentRoom: roomId, chatActiveUser: friendId });
  };

  render() {
    const currentUserId = this.state.currentUser;
    const chatUserId = this.state.chatActiveUser;
    return this.state.currentUser ? (
      <div className="App">
        <header className="App-header">
          <p className="logo"> Chat App</p>
        </header>
        <div className="App-container">
          <FriendList
            chatRoom={id => this.initChatRoom(id)}
            currentUser={this.state.currentUser}
            friendsList={this.state.friendsList}
          />
          <ChatContainer
            loggedInUser={this.state.friendsList[currentUserId]}
            room={this.state.currentRoom}
            chatActiveUser={this.state.friendsList[chatUserId]}
            messages={this.state.messages}
            onsend={this.doSend}
          />
        </div>
      </div>
    ) : (
      <InitPage handleUserChange={event => this.handleUserChange(event)} />
    );
  }
}

// Utility Method to Create Message ID
function makeid() {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < 10; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

// Utility Method to create ChatRoomID
function generateRoomId(friendId, myId) {
  if (Number(friendId) < Number(myId)) {
    return "roomid" + friendId + myId;
  } else {
    return "roomid" + myId + friendId;
  }
}
