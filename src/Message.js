import React, { Component } from "react";
import "./App.css";

export default class Message extends Component {
  render() {
    // Was the message sent by the current user. If so, add a css class
    const mine = this.props.isMine ? "mine" : "";
    return (
      <div className={`message ${mine}`}>
        {/* <div className="username">{this.props.username}</div> */}
        <div className="message-body">{this.props.message}</div>
      </div>
    );
  }
}
