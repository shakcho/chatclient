import React, { Component } from "react";
import "./App.css";
import Message from "./Message";
export default class Messages extends Component {
  render() {
    return (
      <div className="messages">
        {this.props.messages
          .filter(message => message.roomId === this.props.currentRoom)
          .map(message => {
            const isMine = message.sender === this.props.curerentUserId;
            return (
              <Message
                key={message.id.toString()}
                username={message.username}
                message={message.message}
                isMine={isMine}
              />
            );
          })}
      </div>
    );
  }
}
