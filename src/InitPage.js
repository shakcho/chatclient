import React, { Component } from "react";
import "./App.css";

export default class InitPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: ""
    };
  }

  handleChange = event => {
    event.preventDefault();
    if (event.target.value.length > 0) {
      let userId = Number(event.target.value);
      if (userId >= 0 && userId <= 4) {
        this.setState({
          error: ""
        });
        this.props.handleUserChange(userId.toString());
      } else {
        this.setState({
          error: "Please Enter a number between 0 to 4"
        });
      }
    } else {
      this.setState({
        error: ""
      });
    }
  };

  render() {
    return (
      <div className="popover">
        <p className="note">
          <strong>NOTE:</strong>
          This is only for the purpose of selecting a currently Loggedin User,
          as we have not added any login functionality.
          <br />
          Type any number between <strong>0 to 4</strong>, As there are only 5
          members hardcoded in this App.
        </p>
        <input
          className="numberText"
          onChange={event => this.handleChange(event)}
          type="text"
        />
        {this.state.error.length ? (
          <p className="error">{this.state.error}</p>
        ) : (
          ""
        )}
      </div>
    );
  }
}
